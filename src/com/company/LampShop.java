package com.company;

import java.util.*;

public class LampShop {
    private TreeSet<Lamp> lamps;

    public LampShop(){
        this.lamps = new TreeSet<>(new LampComparatorByName());
    }

    public void addLamp(Lamp lamp){
        lamps.add(lamp);
    }

    public void showLamps(){
        System.out.println(lamps);
    }

    public void sortLampsByPowerUsage(){
        TreeSet<Lamp> lampsInNewOrder = new TreeSet<>(new LampComparatorByName());
        lampsInNewOrder.addAll(lamps);
        lamps = lampsInNewOrder;

        System.out.println(lamps);
    }

    public void sortLampsByPower(){
        TreeSet<Lamp> lampsInNewOrder = new TreeSet<>(new LampComparatorByName().reversed());
        lampsInNewOrder.addAll(lamps);
        lamps = lampsInNewOrder;

        System.out.println(lamps);
    }

    public void sortLampsByTime() {
        TreeSet<Lamp> lampsInNewOrder = new TreeSet<>(new LampComparatorByTime().reversed());
        lampsInNewOrder.addAll(lamps);
        lamps = lampsInNewOrder;

        System.out.println(lamps);
    }

    public void sortLampsByName() {
        TreeSet<Lamp> lampsInNewOrder = new TreeSet<>(new LampComparatorByName());
        lampsInNewOrder.addAll(lamps);
        lamps = lampsInNewOrder;

        System.out.println(lamps);
    }

    class LampComparatorByName implements Comparator<Lamp>{
        @Override
        public int compare(Lamp o1, Lamp o2) {
            return o1.getName().compareTo(o2.getName());
        }
    }

    class LampComparatorByTime implements Comparator<Lamp>{
        @Override
        public int compare(Lamp o1, Lamp o2) {
            Integer firstPower = o1.getTime();
            Integer secondPower = o2.getTime();

            return firstPower.compareTo(secondPower);
        }
    }

    class LampComparatorByPower implements Comparator<Lamp>{
        @Override
        public int compare(Lamp o1, Lamp o2) {
            Integer firstPower = o1.getPower();
            Integer secondPower = o2.getPower();

            return firstPower.compareTo(secondPower);
        }

    }

    class LampComparatorByPowerUsage implements Comparator<Lamp>{
        @Override
        public int compare(Lamp o1, Lamp o2) {
            Integer firstPowerUsage = o1.getPower() * o1.getTime();
            Integer secondPowerUsage = o2.getPower() * o2.getTime();

            int result = firstPowerUsage.compareTo(secondPowerUsage);

            if(result == 0){
                Integer firstPower = o1.getPower();
                Integer secondPower = o2.getPower();

                result = firstPower.compareTo(secondPower);
            }

            return result;
        }
    }
}
