package com.company;


public class Lamp{
    int power;
    String name;
    int time;

    public Lamp(int power, String name, int time) {
        this.power = power;
        this.name = name;
        this.time = time;
    }

    public int getPower() {
        return power;
    }

    public String getName() {
        return name;
    }

    public int getTime() {
        return time;
    }

    @Override
    public String toString() {
        return "Lamp{" +
                "power=" + power +
                ", name=" + name +
                ", time=" + time +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if(o instanceof Lamp){
            return this.getName().equals(((Lamp) o).getName());
        }else{
            return false;
        }
    }

    @Override
    public int hashCode() {
        return this.name.hashCode();
    }
}
