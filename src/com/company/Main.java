package com.company;

import jdk.swing.interop.SwingInterOpUtils;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args){
        ArrayList<Lamp> ourLamps = new ArrayList<>();
        ourLamps.add(new Lamp(10, "Danil", 100));
        ourLamps.add(new Lamp(20, "Artem", 90));
        ourLamps.add(new Lamp(5, "Maxim", 95));
        ourLamps.add(new Lamp(50, "Sasha", 30));
        ourLamps.add(new Lamp(15, "Misha", 35));
        ourLamps.add(new Lamp(15, "Misha", 35));
        ourLamps.add(new Lamp(15, "Misha", 35));
        ourLamps.add(new Lamp(100, "Senya", 10));

        LampShop myLampShop = new LampShop();

        for(Lamp lamp : ourLamps){
            myLampShop.addLamp(lamp);
        }

        System.out.println("Our lamps:");
        myLampShop.showLamps();
        System.out.println();

        System.out.println("Sorted by power usage:");
        myLampShop.sortLampsByPowerUsage();
        System.out.println();

        System.out.println("Sorted by power:");
        myLampShop.sortLampsByPower();
        System.out.println();

        System.out.println("Sorted by time");
        myLampShop.sortLampsByTime();
        System.out.println();

        System.out.println("Sorted by name");
        myLampShop.sortLampsByName();
        System.out.println();
    }
}
